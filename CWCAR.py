import RPi.GPIO as GPIO
import time

halfstep_seq = [
[1,0,0,0],
[1,1,0,0],
[0,1,0,0],
[0,1,1,0],
[0,0,1,0],
[0,0,1,1],
[0,0,0,1],
[1,0,0,1]
]
def sensoren():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(16,GPIO.IN)
    GPIO.setup(21,GPIO.IN)
    GPIO.setup(12,GPIO.IN)

    achter = 1
    links = 0
    rechts = 0
        
    prev_input = 0
    
    input = GPIO.input(12)
    if not ((not prev_input) and input):
        rechts = 1
        prev_input = input
        time.sleep(0.005)
    input = GPIO.input(16)
    if not ((not prev_input) and input):
        links = 1
        prev_input = input
        time.sleep(0.005)
    input =  GPIO.input(21)
    if (GPIO.input(21)):
        achter = 0
    return rechts,links,achter



#def ballonkap():

def vooruit(rechts,links,achter):

    
    GPIO.setmode(GPIO.BCM)
    control_pinsL = [2,3,4,17]
    control_pinsR = [9,10,22,27]
    for pin in control_pinsL:
        GPIO.setup(pin, GPIO.OUT)
    for pin in control_pinsR:
        GPIO.setup(pin, GPIO.OUT)
    while achter == 1:
      for halfstep in range(8):
        for pin in range(4):
          GPIO.output(control_pinsL[pin], halfstep_seq[4 - halfstep][pin])
          GPIO.output(control_pinsR[pin], halfstep_seq[4 - halfstep][pin])
        time.sleep(0.001)
        achter = 0
    GPIO.cleanup()


def achteruit():
    GPIO.setmode(GPIO.BCM)
    control_pinsL = [2,3,4,17]
    control_pinsR = [27,22,10,9]
    for pin in control_pinsL:
        GPIO.setup(pin, GPIO.OUT)
    for pin in control_pinsR:
        GPIO.setup(pin, GPIO.OUT)
    while True: 
      for halfstep in range(8):
        for pin in range(4):
          GPIO.output(control_pinsL[pin], halfstep_seq[4 - halfstep][pin])
          GPIO.output(control_pinsR[pin], halfstep_seq[halfstep][pin])
        time.sleep(0.001)
    GPIO.cleanup()

def main():
    while True:
        rechts,links,achter = sensoren()
        vooruit(rechts,links,achter)
    
if __name__ == "__main__":
    main()
