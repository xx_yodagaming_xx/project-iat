import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)


controlPinLeft = [3,5,7,11]
controlPinRight = [13,15,19,21]

for pin in controlPinLeft:
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, 0)

for pin in controlPinRight:
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, 0)
  
#halfstepSeq = [
 #   [1,0,0,0],
  #  [1,1,0,0],
   # [0,1,0,0],
 #   [0,1,1,0],
 #   [0,0,1,0],
  #  [0,0,1,1],
   # [0,0,0,1],
   # [1,0,0,1]
#]

halfstepSeq = [
   [1,0,0,1],
   [0,0,0,1],
   [0,0,1,1],
   [0,0,1,0],
   [0,1,1,0],
   [0,1,0,0],
   [1,1,0,0],
   [1,0,0,0]
]

while True:
    for halfstep in range(8):
        for pin in range(4):
            GPIO.output(controlPinLeft[pin], halfstepSeq[halfstep][pin])
            #GPIO.output(controlPinRight[pin], halfstepSeq[halfstep][pin])
        time.sleep(0.001)
    GPIO.cleanup()


